import Foundation

// Task 1
open class Shape {
    
    open func calculateArea() -> Double {
        fatalError("not implemented")
        
    }
    
    open func calculatePerimeter() -> Double {
        fatalError("not implemented")
        
    }
}

class Rectangle: Shape {
    
    private let width: Double
    private let height: Double
    
    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }
    
    override func calculateArea() -> Double {
        return width * height
    }
    
    override func calculatePerimeter() -> Double {
        return (width + height) * 2
    }
}

class Circle: Shape {
    
    private let radius: Double
    
    init(radius: Double) {
        self.radius = radius
    }
    
    override func calculateArea() -> Double {
        radius * radius * Double.pi
    }
    
    override func calculatePerimeter() -> Double {
        2 * radius * Double.pi
    }
}

class Square: Shape {
    
    private let side: Double
    
    init(side: Double) {
        self.side = side
    }
    
    override func calculateArea() -> Double {
        side * side
    }
    
    override func calculatePerimeter() -> Double {
        side * 4
    }
}

let rectangle = Rectangle(width: 10, height: 20)
let circle = Circle(radius: 5.5)
let square = Square(side: 15)

let shapes: [Shape] = [rectangle, circle, square]
var area: Double = 0
var perimeter: Double = 0

for shape in shapes {
    area += shape.calculateArea()
    perimeter += shape.calculatePerimeter()
}
print("Common area is \(round(area))")
print("Common perimeter is \(round(perimeter))")



//Task 2
func findIndexWithGenerics<T: Equatable>(valueToFind: T, in array: [T]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
if let foundIndexOfString = findIndexWithGenerics(valueToFind: "лама", in: arrayOfString) {
    print("String index: \(foundIndexOfString)")
}

let arrayOfDouble = [5.5, 3.7, 10.1, 7.6, 2.3]
if let foundIndexOfDouble = findIndexWithGenerics(valueToFind: 7.6, in: arrayOfDouble) {
    print("Double index: \(foundIndexOfDouble)")
}

let arrayOfInt = [1, 3, 4, 7, 3]
if let foundIndexOfInt = findIndexWithGenerics(valueToFind: 3, in: arrayOfInt) {
    print("Int index: \(foundIndexOfInt)")
}
