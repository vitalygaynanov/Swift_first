import Foundation

// Homework #1

var name = "Vitaly Gaynanov"
print(name)

//Homework #2

// Задача 1.
let milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

// Задача 2.
var milkPrice: Double = 3

// Задача 3.
milkPrice = 4.20

// Задача 4.
var milkBottleCount: Int? = 20
var profit: Double = 0.0
// Используем безопасное развертывание потому что переменная может содержать nil и тогда программа завершится с ошибкой
if let checkBottles = milkBottleCount {
    profit = milkPrice * Double(checkBottles)
    print(profit)
} else {
    print("Бутылок нет")
}

// Дополнительный пример разворачивания опционала
var result: String? = nil
if let checkResult = result {
    print(checkResult)
} else {
    print("Результата нет")
}

// Задача 5.
var employeesList = ["Иван", "Петр", "Геннадий", "Андрей", "Марфа"]

// Задача 6.
var isEveryoneWorkHard: Bool = false
var workingHours = 39
if workingHours >= 40 {
    isEveryoneWorkHard = true
} else {
    isEveryoneWorkHard = false
}
print(isEveryoneWorkHard)
