// Task 1
var evenNumber: [Int] = []
var n = 0

for number in 0...50 {
    if number % 2 == 0 {
        evenNumber.append(number)
    }
}

while n < 10 {
    print(evenNumber[n])
    n += 1
}



// Task 2
enum Judges: String {
    case firstJudge = "Alex"
    case secondJudge = "Vladimir"
    case thirdJudge = "Mark"
}
typealias Sportsmen = [String:[Judges:Double]]
var sportsmen: Sportsmen = ["Ivan":[.firstJudge:3.5, .secondJudge:4.5, .thirdJudge:5.5], "Petr":[.firstJudge:6.6, .secondJudge:7.7, .thirdJudge:8.8]]

for (sportsman, results) in sportsmen {
    for (judge, score) in results {
        print("Result of \(sportsman) from \(judge.rawValue) is \(score)")
    }
}

for (sportsman, results) in sportsmen {
    var res = 0.0
    for score in results.values {
        res += score
    }
    print("Average score of \(sportsman) is \(res/Double(results.count))")
}
