// Task 1
class Field1 {
    let header: String
    let length: Int
    let placeholder: String
    let code: Int
    let priority: String
    
    init(header: String, length: Int, placeholder: String, code: Int, priority: String) {
        self.header = header
        self.length = length
        self.placeholder = placeholder
        self.code = code
        self.priority = priority
    }
}

let name = Field1(header: "Name", length: 25, placeholder: "Type your name", code: 1, priority: "high")
let surname = Field1(header: "Surname", length: 25, placeholder: "Type your surname", code: 2, priority: "high")
let age = Field1(header: "Age", length: 3, placeholder: "Type your age", code: 3, priority: "middle")
let city = Field1(header: "City", length: 15, placeholder: "City", code: 0, priority: "low")



// Task 2
class Field2 {
    let header: String
    let length: Int
    var placeholder: String
    let code: Int
    let priority: String
    
    init(header: String, length: Int, placeholder: String, code: Int, priority: String) {
        self.header = header
        self.length = length
        self.placeholder = placeholder
        self.code = code
        self.priority = priority
    }
    
    func checkLength() -> Bool {
        return self.length > 0 && self.length <= 25
    }
    
    func editPlaceholder(newPlaceholder: String) {
        self.placeholder = newPlaceholder
    }
}

let longField = Field2(header: "Long", length: 30, placeholder: "Long", code: 1, priority: "middle")
let shortField = Field2(header: "Short", length: 10, placeholder: "Short", code: 1, priority: "middle")
print(longField.checkLength())
print(shortField.checkLength())

let text = Field2(header: "Text", length: 10, placeholder: "Old placeholder", code: 1, priority: "middle")
print(text.placeholder)
text.editPlaceholder(newPlaceholder: "New placeholder")
print(text.placeholder)



// Task 3
class Field3 {
    let header: String
    let length: Int
    var placeholder: String?
    let code: Int?
    let priority: String
    
    init(header: String, length: Int, placeholder: String? = nil, code: Int? = nil, priority: String) {
        self.header = header
        self.length = length
        self.placeholder = placeholder
        self.code = code
        self.priority = priority
    }
    
    func checkLength() -> Bool {
        return self.length > 0 && self.length <= 25
    }
    
    func editPlaceholder(newPlaceholder: String?) {
        self.placeholder = newPlaceholder
    }
}

let optionalField = Field3(header: "Optional", length: 10, priority: "middle")
let optionalFieldWithPlaceholer = Field3(header: "Optional", length: 12, placeholder: "Optional placeholder", priority: "middle")
if let checkField = optionalFieldWithPlaceholer.placeholder {
    print(checkField)
} else {
    print("Field is nil")
}



// Task 4
enum Priority: String {
    case low
    case middle
    case high
}
class Field4 {
    let header: String
    let length: Int
    var placeholder: String?
    let code: Int?
    let priority: String
    
    init(header: String, length: Int, placeholder: String? = nil, code: Int? = nil, priority: Priority) {
        self.header = header
        self.length = length
        self.placeholder = placeholder
        self.code = code
        self.priority = priority.rawValue
    }
    
    func checkLength() -> Bool {
        return self.length > 0 && self.length <= 25
    }
    
    func editPlaceholder(newPlaceholder: String?) {
        self.placeholder = newPlaceholder
    }
}

let newName = Field4(header: "Name", length: 25, placeholder: "Type your name", code: 1, priority: .high)
let newSurname = Field4(header: "Surname", length: 25, placeholder: "Type your surname", code: 2, priority: .high)
let newAge = Field4(header: "Age", length: 3, code: 3, priority: .middle)
let newCity = Field4(header: "City", length: 15, placeholder: "City", priority: .low)



// Task 5
struct Field5 {
    let header: String
    let length: Int
    var placeholder: String?
    var code: Int?
    
    func checkLength() -> Bool {
        return self.length > 0 && self.length <= 25
    }
    
    mutating func editPlaceholder(newPlaceholder: String?) {
        self.placeholder = newPlaceholder
    }
}

let structName = Field5(header: "Name", length: 25, placeholder: "Type your name", code: 1)
let structSurname = Field5(header: "Surname", length: 25, placeholder: "Type your surname", code: 2)
let structAge = Field5(header: "Age", length: 3, code: 3)
let structCity = Field5(header: "City", length: 15, placeholder: "City")
