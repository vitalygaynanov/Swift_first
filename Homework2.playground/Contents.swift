import Foundation

// Задача 1

func fibonacciNumber(count: Int) -> [Int] {
    var number1 = 1
    var number2 = 1
    // Сразу добавим в массив первые два члена ряда Фибоначчи
    var storyPointsNumbers : [Int] = [number1, number2]
    
    if count == 0 || count == 1 {
        return [count]
    }
    
    for _ in 0 ..< count - 2 {
        let number = number1 + number2
        storyPointsNumbers.append(number)
        number1 = number2
        number2 = number
    }
    return storyPointsNumbers
}

print(fibonacciNumber(count: 5))
print(fibonacciNumber(count: 7))
print(fibonacciNumber(count: 0))
print(fibonacciNumber(count: 1))

// Задача 2

let printArrayFor: ([Int]) -> Void = {
    for element in $0 {
        print(element)
    }
}

printArrayFor(fibonacciNumber(count: 7))
