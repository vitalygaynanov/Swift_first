//Task 1
extension Int {
    func sumOfDigitOfNumber() -> Int {
        var number = self
        var sum = 0
        while number > 0 {
            sum += number % 10
            number /= 10
        }
        return sum
    }
}

let number1 = 123
let number2 = 731

print(number1.sumOfDigitOfNumber())
print(number2.sumOfDigitOfNumber())



//Task 2
extension String {
    var makeInt: Int {
        return Int(self) ?? 0
    }
}

print("\"104\"".makeInt)
print("53".makeInt)
print("920".makeInt)
print("Text".makeInt)



//Task 3
protocol Calculatable {
    func calc() -> Double
}

public class Multiply: Calculatable {
    private let firstArgument: Double
    private let secondArgument: Double
    
    init(firstArgument: Double, secondArgument: Double) {
        self.firstArgument = firstArgument
        self.secondArgument = secondArgument
    }
    func calc() -> Double {
        return firstArgument * secondArgument
    }
}

public class Division: Calculatable {
    private let divisible: Double
    private let divider: Double
    
    init(divisible: Double, divider: Double) {
        self.divisible = divisible
        self.divider = divider
    }
    func calc() -> Double {
        return divisible / divider
    }
}

public class Calculator {
    func calc(operation: Calculatable) -> Double {
        return operation.calc()
    }
}

let calculator = Calculator()
let division = Division(divisible: 10, divider: 3)
let result = calculator.calc(operation: division)
print(result)
